CXXFLAG:= g++

output: Main.o Point2D.o
	$(CXXFLAG) Main.o Point2D.o -o output

Main.o: Main.cpp
	$(CXXFLAG) Main.cpp -c

Point2D.o: Point2D.h Point2D.cpp
	$(CXXFLAG) Point2D.cpp -c

clean:
	rm -rf html *.o output *.gch
