/****************************************************************************
 * @file doxygen_c.h
 * @author HTC
 * @brief File contain the defination of 2d point in space
 ****************************************************************************/
#ifndef POINT_2D_H
#define POINT_2D_H
#include <iostream>
class Point2D
{
  public:
    Point2D();
    Point2D(int ,int);
    ~Point2D();
    friend std::istream& operator >> (std::istream& stream, Point2D& point2D);
    friend std::ostream& operator << (std::ostream& stream, const Point2D& point2D);
  private:
    int x;
    int y;
};

#endif