/****************************************************************************
 * @file  Point2D.cpp
 * @author HTC
 * @brief File contain the implementation of Point2D class
 ****************************************************************************/
#include "Point2D.h"
using namespace std;

Point2D::Point2D()
{
    this->x = 0;
    this->y = 0;
}

Point2D::Point2D(int x, int y)
{
    this->x = x;
    this->y = y;
}

Point2D::~Point2D()
{

}

std::ostream& operator << (std::ostream& stream, const Point2D& point2D)
{
    stream << point2D.x << " " << point2D.y;
    return stream;
}

std::istream& operator >> (std::istream& stream, Point2D& point2D)
{
    cout << "Enter x: ";
    stream >> point2D.x;

    cout << "Enter y: ";
    stream >> point2D.y;
    return stream;

}
